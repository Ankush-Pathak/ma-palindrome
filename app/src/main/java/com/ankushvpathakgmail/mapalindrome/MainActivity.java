package com.ankushvpathakgmail.mapalindrome;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText editTextInput;
    TextView textViewResult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextInput = findViewById(R.id.editTextInput);
        textViewResult = findViewById(R.id.textViewResult);

        editTextInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0)
                    textViewResult.setText("");
                if (checkPlaindrome(charSequence) || charSequence.toString().trim().equalsIgnoreCase("palindrome")) {
                    textViewResult.setText("String is palindrome.");
                    textViewResult.setTextColor(Color.GREEN);
                } else {
                    textViewResult.setText("String is not a palindrome.");
                    textViewResult.setTextColor(Color.RED);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() == 0)
                    textViewResult.setText("");
            }
        });
    }

    boolean checkPlaindrome(CharSequence charSequence) {
        String reverseInput = "", input = charSequence.toString().trim();


        for (int i = charSequence.length() - 1; i >= 0; i--) {
            reverseInput = reverseInput + String.valueOf(charSequence.charAt(i));
        }
        return reverseInput.trim().equalsIgnoreCase(input);
    }
}
